//google maps
function initMap() {
  var styledMapType = new google.maps.StyledMapType(
		[
{
	"elementType": "geometry",
	"stylers": [
		{
			"color": "#f5f5f5"
		}
	]
},
{
	"elementType": "labels.icon",
	"stylers": [
		{
			"visibility": "off"
		}
	]
},
{
	"elementType": "labels.text.fill",
	"stylers": [
		{
			"color": "#616161"
		}
	]
},
{
	"elementType": "labels.text.stroke",
	"stylers": [
		{
			"color": "#f5f5f5"
		}
	]
},
{
	"featureType": "administrative.land_parcel",
	"elementType": "labels.text.fill",
	"stylers": [
		{
			"color": "#bdbdbd"
		}
	]
},
{
	"featureType": "landscape.man_made",
	"stylers": [
		{
			"color": "#d0d0d0"
		}
	]
},
{
	"featureType": "landscape.natural",
	"stylers": [
		{
			"color": "#e9e9e9"
		}
	]
},
{
	"featureType": "poi",
	"elementType": "geometry",
	"stylers": [
		{
			"color": "#eeeeee"
		}
	]
},
{
	"featureType": "poi",
	"elementType": "labels.text.fill",
	"stylers": [
		{
			"color": "#757575"
		}
	]
},
{
	"featureType": "poi.park",
	"elementType": "geometry",
	"stylers": [
		{
			"color": "#e5e5e5"
		}
	]
},
{
	"featureType": "poi.park",
	"elementType": "labels.text.fill",
	"stylers": [
		{
			"color": "#9e9e9e"
		}
	]
},
{
	"featureType": "road",
	"elementType": "geometry",
	"stylers": [
		{
			"color": "#ffffff"
		}
	]
},
{
	"featureType": "road.arterial",
	"elementType": "labels.text.fill",
	"stylers": [
		{
			"color": "#757575"
		}
	]
},
{
	"featureType": "road.highway",
	"elementType": "geometry",
	"stylers": [
		{
			"color": "#dadada"
		}
	]
},
{
	"featureType": "road.highway",
	"elementType": "labels.text.fill",
	"stylers": [
		{
			"color": "#616161"
		}
	]
},
{
	"featureType": "road.local",
	"elementType": "labels.text.fill",
	"stylers": [
		{
			"color": "#9e9e9e"
		}
	]
},
{
	"featureType": "transit.line",
	"elementType": "geometry",
	"stylers": [
		{
			"color": "#e5e5e5"
		}
	]
},
{
	"featureType": "transit.station",
	"elementType": "geometry",
	"stylers": [
		{
			"color": "#eeeeee"
		}
	]
},
{
	"featureType": "water",
	"elementType": "geometry",
	"stylers": [
		{
			"color": "#ffb75e"
		}
	]
},
{
	"featureType": "water",
	"elementType": "labels.text.fill",
	"stylers": [
		{
			"color": "#9e9e9e"
		}
	]
}
],
      {name: "Styled Map"});

    var cords = {lat: 50.4430981 , lng: 16.8745081 };
    var map = new google.maps.Map(document.getElementById('map'), {
      center: cords,
      zoom: 15,
      mapTypeControlOptions: {
        mapTypeIds: ['styled_map']
      }
    });

    //connecting styled map with mapTypeIds
    map.mapTypes.set('styled_map', styledMapType);
    map.setMapTypeId('styled_map');

    var marker = new google.maps.Marker({
      map: map,
      position: cords,
      title: 'Urząd Miasta Krosna'
    });
  }


;(function () {

	'use strict';



	var isMobile = {
		Android: function() {
			return navigator.userAgent.match(/Android/i);
		},
			BlackBerry: function() {
			return navigator.userAgent.match(/BlackBerry/i);
		},
			iOS: function() {
			return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		},
			Opera: function() {
			return navigator.userAgent.match(/Opera Mini/i);
		},
			Windows: function() {
			return navigator.userAgent.match(/IEMobile/i);
		},
			any: function() {
			return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		}
	};


	var testimonialCarousel = function(){

		var owl = $('.owl-carousel-fullwidth');
		owl.owlCarousel({
			items: 1,
			loop: true,
			margin: 0,
			responsiveClass: true,
			nav: false,
			dots: true,
			autoHeight: true,
			smartSpeed: 800,
			autoHeight: true
		});

	};

	// var contentWayPoint = function() {
	// 	var i = 0;
	// 	$('.animate-box').waypoint( function( direction ) {
	//
	// 		if( direction === 'down' && !$(this.element).hasClass('animated-fast') ) {
	//
	// 			i++;
	//
	// 			$(this.element).addClass('item-animate');
	// 			setTimeout(function(){
	//
	// 				$('body .animate-box.item-animate').each(function(k){
	// 					var el = $(this);
	// 					setTimeout( function () {
	// 						var effect = el.data('animate-effect');
	// 						if ( effect === 'fadeIn') {
	// 							el.addClass('fadeIn animated-fast');
	// 						} else if ( effect === 'fadeInLeft') {
	// 							el.addClass('fadeInLeft animated-fast');
	// 						} else if ( effect === 'fadeInRight') {
	// 							el.addClass('fadeInRight animated-fast');
	// 						} else {
	// 							el.addClass('fadeInUp animated-fast');
	// 						}
	//
	// 						el.removeClass('item-animate');
	// 					},  k * 200, 'easeInOutExpo' );
	// 				});
	//
	// 			}, 100);
	//
	// 		}
	//
	// 	} , { offset: '85%' } );
	// };
	//
	//
	//
	//
	// $(function(){
	// 	testimonialCarousel();
	// 	contentWayPoint();
	// });

//sidebar button

	$("#menu-toggle").click(function(e) {
			e.preventDefault();
			$("#wrapper").toggleClass("toggled");
			$("#menu-toggle").toggleClass("toggled");
	});

//smooth scrolling

$(document).ready( function() {
	$('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
	if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
	  var target = $(this.hash);
	  target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
	  if (target.length) {
	    $('html, body').animate({
	      scrollTop: (target.offset().top - 10)
	    }, 1000, "easeInOutExpo");
	    return false;
	  }
	}
	});
});

// read more

$("#show").click(function(){
  $('.hidden').show('slow');
});

console.log('test')

}());
