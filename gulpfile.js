var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');

// sass > css
gulp.task('sass', function() {
    return gulp.src(['node_modules/bootstrap/dist/css/bootstrap.css',
    'node_modules/animate.css/animate.min.css', 'src/scss/*.scss',
    'node_modules/owlcarousel/owl-carousel/owl.carousel.css', 'node_modules/owlcarousel/owl-carousel/owl.theme.css'])
        .pipe(sass())
        .pipe(gulp.dest("src/css"))
        .pipe(browserSync.stream());
});

// Move the javascript files into our /src/js folder
gulp.task('js', function() {
    return gulp.src(['node_modules/bootstrap/dist/js/bootstrap.min.js',
    'node_modules/scrollreveal/dist/scrollreveal.js', 'node_modules/jquery/dist/jquery.min.js',
    'node_modules/jquery.easing/jquery.easing.js', 'node_modules/modernizr/src/Modernizr.js',
    'node_modules/owlcarousel/owl-carousel/owl.carousel.min.js', 'node_modules/jquery-waypoints/waypoints.min.js'
    ])
        .pipe(gulp.dest("src/js"))
        .pipe(browserSync.stream());
});

// Static Server + watching scss/html files
gulp.task('serve', ['sass'], function() {

    browserSync.init({
        server: "./src"
    });

    gulp.watch(['node_modules/bootstrap/dist/css/bootstrap.css', 'src/scss/*.scss'], ['sass']);
    gulp.watch(['src/*.html', "src/css/style-purify.css"]).on('change', browserSync.reload);
});

gulp.task('default', ['js','serve']);
